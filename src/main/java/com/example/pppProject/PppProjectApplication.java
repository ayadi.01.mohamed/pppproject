package com.example.pppProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PppProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PppProjectApplication.class, args);
	}

}
